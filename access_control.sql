CREATE TABLE IF NOT EXISTS users (
    id UUID PRIMARY KEY,
    username VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS rolle (
    id UUID PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS permissions (
    id UUID PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL
);

INSERT INTO users (id, username, password) VALUES ('550e8400-e29b-41d4-a716-446655440000', 'anam1', 'anam123'), ('123e4567-e89b-12d3-a456-426614174001', 'anam2', 'anam123');
INSERT INTO rolle (id, name) VALUES ('7acf58a3-4a86-4574-b3c1-d1a5175ad480', 'admin'), ('7acf58a3-4a86-4574-b3c1-d1a5175ad480', 'user');
INSERT INTO permissions (id, name) VALUES ('6a7a0fe3-080d-4190-9af7-8b9f332e4c47', 'read'), ('d2b690b4-f7b9-4b26-83a8-9e1e9a6b2a8d', 'write');
